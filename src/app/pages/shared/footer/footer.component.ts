import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-footer",
	templateUrl: "./footer.component.html",
	styleUrls: ["./footer.component.css"],
})
export class FooterComponent implements OnInit {
	constructor() {}

	navLinks: any[] = [
		{
			name: "Shop",
			link: "/shop",
		},
		{
			name: "Blog",
			link: "/blog",
		},
		{
			name: "Contact Us",
			link: "/contact",
		},
	];

	Social: any[] = [
		{
			link: "https://www.facebook.com/RandRluxury/",
			icon: "fa-facebook-square",
		},
		{
			link: "https://twitter.com/@randrluxury",
			icon: "fa-twitter-square",
		},
		{
			link: "https://www.instagram.com/randrluxury/",
			icon: "fa-instagram-square",
		},
	];

	ngOnInit(): void {}
}
