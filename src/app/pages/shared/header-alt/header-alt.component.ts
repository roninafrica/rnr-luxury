import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
	selector: "app-header-alt",
	templateUrl: "./header-alt.component.html",
	styleUrls: ["./header-alt.component.css"],
})
export class HeaderAltComponent implements OnInit {
	constructor() {}
	nav = {
		logo: "assets/img/logo-alt.png",
	};

	navLinks: any[] = [
		{
			name: "Home",
			link: "/home",
		},
		{
			name: "About Us",
			link: "/about_us",
		},
		{
			name: "Shop",
			link: "/shop",
		},
		{
			name: "Blog",
			link: "/blog",
		},
		{
			name: "Contact Us",
			link: "/contact",
		},
	];

	public isCollapsed = true;
	ngOnInit() {
		$(document).ready(function () {
			$(window).scroll(function () {
				var scroll = $(window).scrollTop();
				if (scroll > 0) {
					$(".navbar").addClass("navbar-scroll");
				} else {
					$(".navbar").removeClass("navbar-scroll");
				}
				if (scroll > 200) {
					$(".navbar").addClass("bg-main");
				} else {
					$(".navbar").removeClass("bg-main");
				}
			});
		});
	}
}
