import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    (<any>$)(document).ready(function () {
      var secondaryNav = (<any>$)('.cd-secondary-nav'),
        secondaryNavTopPosition = secondaryNav.offset().top,
        taglineOffesetTop =
          (<any>$)('#cd-intro-tagline').offset().top +
          (<any>$)('#cd-intro-tagline').height() +
          parseInt((<any>$)('#cd-intro-tagline').css('paddingTop').replace('px', '')),
        contentSections = (<any>$)('.cd-section');

      (<any>$)(window).on('scroll', function () {
        //on desktop - assign a position fixed to logo and action button and move them outside the viewport
        (<any>$)(window).scrollTop() > taglineOffesetTop
          ? (<any>$)('#cd-logo, .cd-btn').addClass('is-hidden')
          : (<any>$)('#cd-logo, .cd-btn').removeClass('is-hidden');

        //on desktop - fix secondary navigation on scrolling
        if ((<any>$)(window).scrollTop() > secondaryNavTopPosition) {
          //fix secondary navigation
          secondaryNav.addClass('is-fixed');
          //push the .cd-main-content giving it a top-margin
          (<any>$)('.cd-main-content').addClass('has-top-margin');
          //on Firefox CSS transition/animation fails when parent element changes position attribute
          //so we to change secondary navigation childrens attributes after having changed its position value
          setTimeout(function () {
            secondaryNav.addClass('animate-children');
            (<any>$)('#cd-logo').addClass('slide-in');
            (<any>$)('.cd-btn').addClass('slide-in');
          }, 50);
        } else {
          secondaryNav.removeClass('is-fixed');
          (<any>$)('.cd-main-content').removeClass('has-top-margin');
          setTimeout(function () {
            secondaryNav.removeClass('animate-children');
            (<any>$)('#cd-logo').removeClass('slide-in');
            (<any>$)('.cd-btn').removeClass('slide-in');
          }, 50);
        }

        //on desktop - update the active link in the secondary fixed navigation
        updateSecondaryNavigation();
      });

      function updateSecondaryNavigation() {
        contentSections.each(function () {
          var actual = (<any>$)(this),
            actualHeight =
              actual.height() +
              parseInt(actual.css('paddingTop').replace('px', '')) +
              parseInt(actual.css('paddingBottom').replace('px', '')),
            actualAnchor = secondaryNav.find(
              ''
            );
          if (
            actual.offset().top - secondaryNav.height() <=
              (<any>$)(window).scrollTop() &&
            actual.offset().top + actualHeight - secondaryNav.height() >
              (<any>$)(window).scrollTop()
          ) {
            actualAnchor.addClass('active');
          } else {
            actualAnchor.removeClass('active');
          }
        });
      }

      //on mobile - open/close secondary navigation clicking/tapping the .cd-secondary-nav-trigger
      (<any>$)('.cd-secondary-nav-trigger').on('click', function (event) {
        event.preventDefault();
        (<any>$)(this).toggleClass('menu-is-open');
        secondaryNav.find('ul').toggleClass('is-visible');
      });

      //smooth scrolling when clicking on the secondary navigation items
      secondaryNav.find('ul a').on('click', function (event) {
        event.preventDefault();
        var target = (<any>$)(this.hash);
        (<any>$)('body,html').animate(
          {
            scrollTop: target.offset().top - secondaryNav.height() + 1,
          },
          400
        );
        //on mobile - close secondary navigation
        (<any>$)('.cd-secondary-nav-trigger').removeClass('menu-is-open');
        secondaryNav.find('ul').removeClass('is-visible');
      });

      //on mobile - open/close primary navigation clicking/tapping the menu icon
      (<any>$)('.cd-primary-nav').on('click', function (event) {
        if ((<any>$)(event.target).is('.cd-primary-nav'))
          (<any>$)(this).children('ul').toggleClass('is-visible');
      });
    });
  }

}
