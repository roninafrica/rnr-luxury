import { Component, OnInit } from "@angular/core";
import { SeoService } from "src/app/services/seo.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "app-shoppage",
	templateUrl: "./shoppage.component.html",
	styleUrls: ["./shoppage.component.css"],
})
export class ShoppageComponent implements OnInit {
	seoData: any = {
		title: "Shop | RnR Luxury Ghana",
		description: "",
		keywords: "",
	};

	shop = {
		title: "Shop",
		subtitle:
			"Shopping online or inPerson, Select the country to find a shop near you",
	};

	constructor(private seo: SeoService, private modalService: NgbModal) {
		this.seo.setSeo(this.seoData);
	}

	modalTabs(tabContent) {
		this.modalService.open(tabContent, {
			backdrop: "static",
		});
	}


	ngOnInit() {}
}
