import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';

@Component({
	selector: "app-productspage",
	templateUrl: "./productspage.component.html",
	styleUrls: ["./productspage.component.css"],
})
export class ProductspageComponent implements OnInit {
	seoData: any = {
		title: "Our Products | RnR Luxury Ghana",
		description: "",
		keywords: "",
	};

	product = {
		title: "Our Products",
		subtitle: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
