import { Component, OnInit } from "@angular/core";
import { SeoService } from "src/app/services/seo.service";
@Component({
	selector: "app-blogpage",
	templateUrl: "./blogpage.component.html",
	styleUrls: ["./blogpage.component.css"],
})
export class BlogpageComponent implements OnInit {
	seoData: any = {
		title: "Our Blog | RnR Luxury Ghana",
		description: "",
		keywords: "",
	};

	blog = {
		title: "Our Blog Posts",
		subtitle: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
	};

	Articles: any[] = [
		{
			id: "1",
			categoryName: "Food",
			categoryLink: "",
			title: "Ask HN: Does Anybody Still Use Shea Butter?",
			summary:
				"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
			author: "Serwaa Akoto",
			date: "27th March, 2020",
			image: "assets/img/1.jpg",
		},
		{
			id: "3",
			categoryName: "Service",
			categoryLink: "",
			title: "Why the rush, Bro?",
			summary:
				"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
			author: "June Fare",
			date: "27th July, 2020",
			image: "assets/img/3.jpg",
		},
		{
			id: "2",
			categoryName: "Lifestyle",
			categoryLink: "",
			title: "Where Do We Come From",
			summary:
				"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
			author: "Kofi Dagaati",
			date: "1st December, 2021",
			image: "assets/img/2.jpg",
		},
	];

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
