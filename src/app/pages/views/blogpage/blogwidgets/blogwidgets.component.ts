import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-blogwidgets",
	templateUrl: "./blogwidgets.component.html",
	styleUrls: ["./blogwidgets.component.css"],
})
export class BlogwidgetsComponent implements OnInit {
	category: any[] = [
		{
			name: "Service",
			number: "12",
			link: "",
		},
		{
			name: "Food",
			number: "3",
			link: "",
		},
		{
			name: "Lifestyle",
			number: "9",
			link: "",
		},
	];

	tags: any[] = [
		{
			name: "Service",
			link: "",
		},
		{
			name: "Food",
			link: "",
		},
		{
			name: "Lifestyle",
			link: "",
		},
		{
			name: "Festivals",
			link: "",
		},
		{
			name: "Kokrokoo",
			link: "",
		},
		{
			name: "Family",
			link: "",
		},
	];

	constructor() {}

	ngOnInit(): void {}
}
