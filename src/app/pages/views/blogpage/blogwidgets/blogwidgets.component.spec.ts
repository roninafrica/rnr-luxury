import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogwidgetsComponent } from './blogwidgets.component';

describe('BlogwidgetsComponent', () => {
  let component: BlogwidgetsComponent;
  let fixture: ComponentFixture<BlogwidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogwidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogwidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
