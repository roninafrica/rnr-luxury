import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from "src/app/services/seo.service";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: "app-article",
	templateUrl: "./article.component.html",
	styleUrls: ["./article.component.css"],
})
export class ArticleComponent implements OnInit {
	seoData: any = {
		title: "Our Blog | RnR Luxury Ghana",
		description: "",
		keywords: "",
	};

	article = {
		title: "Article Title",
		subtitle: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
	};
	constructor(private seo: SeoService, private route: ActivatedRoute) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
