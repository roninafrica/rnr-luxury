import { Component, OnInit } from '@angular/core';
import { SeoService } from "src/app/services/seo.service";

@Component({
	selector: "app-aboutpage",
	templateUrl: "./aboutpage.component.html",
	styleUrls: ["./aboutpage.component.css"],
})
export class AboutpageComponent implements OnInit {
	seoData: any = {
		title: "About Us | RnR Luxury Ghana",
		description: "",
		keywords: "",
	};

	about = {
		title: "Our Story",
		subtitle: "Lorem ipsum dolor sit, amet consectetur adipisicing elit.",
	};

	constructor(private seo: SeoService) {
		this.seo.setSeo(this.seoData);
	}

	ngOnInit(): void {}
}
