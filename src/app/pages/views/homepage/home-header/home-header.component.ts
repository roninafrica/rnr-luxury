import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-home-header",
	templateUrl: "./home-header.component.html",
	styleUrls: ["./home-header.component.css"],
})
export class HomeHeaderComponent implements OnInit {
	slideData: any[] = [
		{
			title: "Title 1",
			subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/img/s1.jpg",
			buttonText: "",
			buttonLink: "",
		},
		{
			title: "Title 2",
			subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/img/s2.jpg",
			buttonText: "",
			buttonLink: "",
		},
		{
			title: "Title 3",
			subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/img/s3.jpg",
			buttonText: "",
			buttonLink: "",
		},
		{
			title: "Title 4",
			subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/img/s4.jpg",
			buttonText: "",
			buttonLink: "",
		},
	];

	constructor() {}

	ngOnInit() {}
}
