import { Component, OnInit } from "@angular/core";
import { SeoService } from "src/app/services/seo.service";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.css"],
})
export class HomepageComponent implements OnInit {
  seoData: any = {
    title: "Home | RnR Luxury Ghana",
    description: "",
    keywords: "",
  };

  constructor(private seo: SeoService) {
    this.seo.setSeo(this.seoData);
  }

  ngOnInit(): void {}
}
