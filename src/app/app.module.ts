import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./pages/shared/header/header.component";
import { FooterComponent } from "./pages/shared/footer/footer.component";
import { HomepageComponent } from "./pages/views/homepage/homepage.component";
import { AboutpageComponent } from "./pages/views/aboutpage/aboutpage.component";
import { ProductspageComponent } from "./pages/views/productspage/productspage.component";
import { BlogpageComponent } from "./pages/views/blogpage/blogpage.component";
import { ContactpageComponent } from "./pages/views/contactpage/contactpage.component";
import { PagenotfoundComponent } from "./pages/views/pagenotfound/pagenotfound.component";
import { HeaderAltComponent } from "./pages/shared/header-alt/header-alt.component";
import { PostComponent } from "./pages/views/blogpage/post/post.component";
import { ArticleComponent } from "./pages/views/blogpage/article/article.component";
import { SubscribeComponent } from "./pages/views/subscribe/subscribe.component";
import { HomeHeaderComponent } from './pages/views/homepage/home-header/home-header.component';
import { ItemComponent } from './pages/views/productspage/item/item.component';
import { ItemDetailsComponent } from './pages/views/productspage/item-details/item-details.component';
import { ShoppageComponent } from './pages/views/shoppage/shoppage.component';
import { BlogwidgetsComponent } from './pages/views/blogpage/blogwidgets/blogwidgets.component';

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent,
		HomepageComponent,
		AboutpageComponent,
		ProductspageComponent,
		BlogpageComponent,
		ContactpageComponent,
		PagenotfoundComponent,
		HeaderAltComponent,
		PostComponent,
		ArticleComponent,
		SubscribeComponent,
		HomeHeaderComponent,
		ItemComponent,
		ItemDetailsComponent,
		ShoppageComponent,
		BlogwidgetsComponent,
	],
	imports: [BrowserModule, AppRoutingModule, NgbModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
