import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/views/homepage/homepage.component';
import { AboutpageComponent } from './pages/views/aboutpage/aboutpage.component';
import { ProductspageComponent } from './pages/views/productspage/productspage.component';
import { BlogpageComponent } from './pages/views/blogpage/blogpage.component';
import { ContactpageComponent } from './pages/views/contactpage/contactpage.component';
import { PagenotfoundComponent } from './pages/views/pagenotfound/pagenotfound.component';
import { ArticleComponent } from './pages/views/blogpage/article/article.component';
import { ShoppageComponent } from './pages/views/shoppage/shoppage.component'


const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'home',
    component: HomepageComponent,
  },
  {
    path: 'about_us',
    component: AboutpageComponent,
  },
  {
    path: 'shop',
    children: [
      {
        path: '', // child route path
        component: ShoppageComponent,
      },
    ],
  },

  // Muted Product Page Route
  // {
  //   path: 'products',
  //   component: ProductspageComponent,
  // },
  {
    path: 'blog',
    children: [
      {
        path: '', // child route path
        component: BlogpageComponent,
      },
      // {
      //   path: 'article', // child route path
      //   component: ArticleComponent,
      // },
    ],
  },
  {
    path: 'contact',
    component: ContactpageComponent,
  },
  {
    path: '**',
    component: PagenotfoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
