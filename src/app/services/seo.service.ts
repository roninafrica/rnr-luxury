import { Meta, Title } from "@angular/platform-browser";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class SeoService {
  constructor(private titleService: Title, private meta: Meta) {}

  setSeo(data: any) {
    this.titleService.setTitle(data.title);
    this.meta.updateTag({
      name: data.title || "R&R Luxury",
      content:
        data.description ||
        "R&R is an entirely natural,  plant-based skincare brand from Africa, founded on the philosophy of transforming one of nature’s purest and richest gifts - Shea Butter – into elegant beauty products that moisturise and renew the skin.",
      author: "RMQ | Ronin Africa",
    });
    this.meta.updateTag({ name: "keywords", content: data.keyboards });
    this.meta.updateTag({ name: "author", content: data.author });
  }
}
